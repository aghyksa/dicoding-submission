package com.aghyksa.dicodingsubmissionbfaa.di.ui.main

import dagger.Module

// Main Module; Keyword : Dagger2
@Module
object MainModule