package com.aghyksa.dicodingsubmissionbfaa.di.ui.main

import com.aghyksa.dicodingsubmissionbfaa.di.HomeScope
import com.aghyksa.dicodingsubmissionbfaa.di.ProfileScope
import com.aghyksa.dicodingsubmissionbfaa.di.ui.main.home.HomeModule
import com.aghyksa.dicodingsubmissionbfaa.di.ui.main.profile.ProfileModule
import com.aghyksa.dicodingsubmissionbfaa.ui.main.home.HomeFragment
import com.aghyksa.dicodingsubmissionbfaa.ui.main.profile.ProfileFragment
import com.aghyksa.dicodingsubmissionbfaa.ui.main.profile.follow.ProfileFollowFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

// Main Activity Builder Module; Keyword : Dagger2
@Module
abstract class MainActivityBuilderModule {

    @HomeScope
    @ContributesAndroidInjector(
        modules = [
            HomeModule::class
        ]
    )
    abstract fun contributeHomeFragment(): HomeFragment

    @ProfileScope
    @ContributesAndroidInjector(
        modules = [
            ProfileModule::class
        ]
    )
    abstract fun contributeProfileFragment(): ProfileFragment

    @ProfileScope
    @ContributesAndroidInjector(
        modules = [
            ProfileModule::class
        ]
    )
    abstract fun contributeProfileFollowFragment(): ProfileFollowFragment
}