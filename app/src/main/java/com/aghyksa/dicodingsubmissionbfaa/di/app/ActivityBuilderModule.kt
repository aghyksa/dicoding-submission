package com.aghyksa.dicodingsubmissionbfaa.di.app

import com.aghyksa.dicodingsubmissionbfaa.di.ui.main.MainActivityBuilderModule
import com.aghyksa.dicodingsubmissionbfaa.di.ui.main.MainModule
import com.aghyksa.dicodingsubmissionbfaa.di.ui.main.MainViewModelModule
import com.aghyksa.dicodingsubmissionbfaa.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

// Activity Builder Module; Keyword : Dagger2
@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(
        modules = [
            MainActivityBuilderModule::class,
            MainViewModelModule::class,
            MainModule::class
        ]
    )
    abstract fun contributeMainActivity(): MainActivity

}