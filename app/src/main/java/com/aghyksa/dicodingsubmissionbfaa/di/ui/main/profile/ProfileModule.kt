package com.aghyksa.dicodingsubmissionbfaa.di.ui.main.profile

import com.aghyksa.dicodingsubmissionbfaa.data.network.api.ApiHelper
import com.aghyksa.dicodingsubmissionbfaa.data.repositories.ProfileRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

// Main - Profile Module; Keyword : Dagger2
@Module
object ProfileModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideProfileRepository(
        api: ApiHelper
    ) = ProfileRepository(api)
}