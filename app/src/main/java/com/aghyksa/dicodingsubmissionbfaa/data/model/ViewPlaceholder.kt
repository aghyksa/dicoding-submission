package com.aghyksa.dicodingsubmissionbfaa.data.model

import com.aghyksa.dicodingsubmissionbfaa.R

// Data Class Placeholder for View; Keyword : DataClass
data class ViewPlaceholder(
    var show: Boolean? = null,
    var image: Int? = R.drawable.il_home_search_error,
    var tittle: Int? = R.string.placeholder_error_tittle,
    var message: Int? = R.string.placeholder_error_message
)